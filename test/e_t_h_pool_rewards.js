const ETHPoolRewards = artifacts.require("ETHPoolRewards");

/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
const DEFAULT_ADMIN_ROLE = '0x0000000000000000000000000000000000000000000000000000000000000000';
const TEAM_MEMBER_ROLE = web3.utils.soliditySha3('ETHPOOL_TEAM_MEMBER');


contract("ETHPoolRewards", function (accounts) {
    let [deployer, alice, bob, teamMember1] = accounts;
    let instance;

    describe("Team membership management", () => {
        before('Setup contract', async () => {
            instance = await ETHPoolRewards.new();
        });
        it("deployer should have default admin role", async () => {
            const hasRole = await instance.hasRole(DEFAULT_ADMIN_ROLE, deployer, {from: deployer});
            assert.equal(hasRole, true);
        });
        it("deployer should be able to add team member", async () => {
            await instance.grantRole(TEAM_MEMBER_ROLE, teamMember1, {from: deployer});
            const hasRole = await instance.hasRole(TEAM_MEMBER_ROLE, teamMember1, {from: deployer});
            assert.equal(hasRole, true);
        });
        it("deployer should be able to remove a team member", async () => {
            await instance.revokeRole(TEAM_MEMBER_ROLE, teamMember1, {from: deployer});
            const hasRole = await instance.hasRole(TEAM_MEMBER_ROLE, teamMember1, {from: deployer});
            assert.equal(hasRole, false);
        });
    });
    describe("Rewards after deposits", () => {
        before('Setup contract', async () => {
            instance = await ETHPoolRewards.new();
        });
        it("should let alice to deposit 100", async () => {
            await instance.deposit({value: 100, from: alice});
            const balance = await instance.holderBalances.call(alice);
            const totalDeposits = await instance.totalDeposits.call();
            assert.equal(totalDeposits, 100);
            assert.equal(balance.deposits, 100);
        });
        it("should let bob to deposit 300", async () => {
            await instance.deposit({value: 300, from: bob});
            const balance = await instance.holderBalances.call(bob);
            const totalDeposits = await instance.totalDeposits.call();
            assert.equal(balance.deposits, 300);
            assert.equal(totalDeposits, 400);
        });
        it("should let team member to reward with 200 and each holder receives their proportion", async () => {
            await instance.grantRole(TEAM_MEMBER_ROLE, teamMember1, {from: deployer});
            await instance.reward({value: 200, from: teamMember1});
            const aliceBalance = await instance.holderBalances.call(alice);
            const bobBalance = await instance.holderBalances.call(bob);
            assert.equal(aliceBalance.rewards, 50);
            assert.equal(bobBalance.rewards, 150);
        });
        it("should let alice to witdhraw the correct amount, clearing her ETHPool deposits and rewards balance", async () => {
            const initialBalance = await web3.eth.getBalance(alice);
            const initialBalanceBN = new web3.utils.BN(initialBalance);
            const {tx, receipt: {gasUsed}} = await instance.withdraw({from: alice});

            const {gasPrice} = await web3.eth.getTransaction(tx);
            const totalGasCost = new web3.utils.BN(gasPrice).mul(new web3.utils.BN(gasUsed));

            const finalBalance = await web3.eth.getBalance(alice);

            const expectedWithdrawAmount = new web3.utils.BN(150);
            const expectedBalance = initialBalanceBN
                .add(expectedWithdrawAmount)
                .sub(totalGasCost);

            assert.equal(finalBalance, expectedBalance, "Alice's balance different than expected");

            const aliceEthPoolBalance = await instance.holderBalances.call(alice);
            assert.equal(aliceEthPoolBalance.deposits,0 , "Alice's ETHPool deposits were not cleared");
            assert.equal(aliceEthPoolBalance.rewards,0, "Alice's ETHPool rewards were not cleared");
        });
        it("should let bob to witdhraw the correct amount, clearing his ETHPool deposits and rewards balance", async () => {
            const initialBalance = await web3.eth.getBalance(bob);
            const initialBalanceBN = new web3.utils.BN(initialBalance);
            const {tx, receipt: {gasUsed}} = await instance.withdraw({from: bob});

            const {gasPrice} = await web3.eth.getTransaction(tx);
            const totalGasCost = new web3.utils.BN(gasPrice).mul(new web3.utils.BN(gasUsed));

            const finalBalance = await web3.eth.getBalance(bob);

            const expectedWithdrawAmount = new web3.utils.BN(450);
            const expectedBalance = initialBalanceBN
                .add(expectedWithdrawAmount)
                .sub(totalGasCost);

            assert.equal(finalBalance, expectedBalance, "Bob's balance different than expected");

            const bobEthPoolBalance = await instance.holderBalances.call(alice);
            assert.equal(bobEthPoolBalance.deposits,0 , "Bob's ETHPool deposits were not cleared");
            assert.equal(bobEthPoolBalance.rewards,0, "Bob's ETHPool rewards were not cleared");
        });
    });

    describe("Reward between deposits", () => {
        before('Setup contract', async () => {
            instance = await ETHPoolRewards.new();
        });
        it("should let alice to deposit 100", async () => {
            await instance.deposit({value: 100, from: alice});
            const balance = await instance.holderBalances.call(alice);
            const totalDeposits = await instance.totalDeposits.call();
            assert.equal(totalDeposits, 100);
            assert.equal(balance.deposits, 100);
        });
        it("should let team member to reward with 200 and only alice receives all the rewards", async () => {
            await instance.grantRole(TEAM_MEMBER_ROLE, teamMember1, {from: deployer});
            await instance.reward({value: 200, from: teamMember1});
            const aliceBalance = await instance.holderBalances.call(alice);
            const bobBalance = await instance.holderBalances.call(bob);
            assert.equal(aliceBalance.rewards, 200);
            assert.equal(bobBalance.rewards, 0);
        });
        it("should let bob to deposit 300", async () => {
            await instance.deposit({value: 300, from: bob});
            const balance = await instance.holderBalances.call(bob);
            const totalDeposits = await instance.totalDeposits.call();
            assert.equal(balance.deposits, 300);
            assert.equal(totalDeposits, 400);
        });

        it("should let alice to witdhraw the correct amount, clearing her ETHPool deposits and rewards balance", async () => {
            const initialBalance = await web3.eth.getBalance(alice);
            const initialBalanceBN = new web3.utils.BN(initialBalance);
            const {tx, receipt: {gasUsed}} = await instance.withdraw({from: alice});

            const {gasPrice} = await web3.eth.getTransaction(tx);
            const totalGasCost = new web3.utils.BN(gasPrice).mul(new web3.utils.BN(gasUsed));

            const finalBalance = await web3.eth.getBalance(alice);

            const expectedWithdrawAmount = new web3.utils.BN(300);
            const expectedBalance = initialBalanceBN
                .add(expectedWithdrawAmount)
                .sub(totalGasCost);

            assert.equal(finalBalance, expectedBalance, "Alice's balance different than expected");

            const aliceEthPoolBalance = await instance.holderBalances.call(alice);
            assert.equal(aliceEthPoolBalance.deposits,0 , "Alice's ETHPool deposits were not cleared");
            assert.equal(aliceEthPoolBalance.rewards,0, "Alice's ETHPool rewards were not cleared");
        });
        it("should let bob to witdhraw the correct amount, clearing his ETHPool deposits and rewards balance", async () => {
            const initialBalance = await web3.eth.getBalance(bob);
            const initialBalanceBN = new web3.utils.BN(initialBalance);
            const {tx, receipt: {gasUsed}} = await instance.withdraw({from: bob});

            const {gasPrice} = await web3.eth.getTransaction(tx);
            const totalGasCost = new web3.utils.BN(gasPrice).mul(new web3.utils.BN(gasUsed));

            const finalBalance = await web3.eth.getBalance(bob);

            const expectedWithdrawAmount = new web3.utils.BN(300);
            const expectedBalance = initialBalanceBN
                .add(expectedWithdrawAmount)
                .sub(totalGasCost);

            assert.equal(finalBalance, expectedBalance, "Bob's balance different than expected");

            const bobEthPoolBalance = await instance.holderBalances.call(alice);
            assert.equal(bobEthPoolBalance.deposits,0 , "Bob's ETHPool deposits were not cleared");
            assert.equal(bobEthPoolBalance.rewards,0, "Bob's ETHPool rewards were not cleared");
        });
    });
});
