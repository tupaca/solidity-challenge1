// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/AccessControl.sol";


contract ETHPoolRewards is AccessControl {
  bytes32 public constant TEAM_MEMBER_ROLE = keccak256("ETHPOOL_TEAM_MEMBER");

  uint public totalDeposits;

  address[] public holderAddresses;
  struct holderBalanceData {
    uint deposits;
    uint rewards;
    uint holderIndex;
    bool isTracked;
  }
  mapping(address => holderBalanceData) public holderBalances;

  event HolderRewarded(address holder, uint rewardAmount);
  event HolderWithdrawal(address holder, uint rewardsAmount, uint depositsAmount);

  constructor() {
    _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
  }


  function deposit() external payable {
    if (!holderBalances[msg.sender].isTracked) {
      holderBalances[msg.sender].isTracked = true;
      holderAddresses.push(msg.sender);
    }
    holderBalances[msg.sender].deposits += msg.value;
    totalDeposits += msg.value;
  }

  function reward() external payable onlyRole(TEAM_MEMBER_ROLE) {
    address[] memory holderAddressesMemory = holderAddresses;
    for (uint i = 0; i < holderAddressesMemory.length; i++) {
      uint holderDeposits = holderBalances[holderAddressesMemory[i]].deposits;
      uint rewardAmount = uint(holderDeposits * msg.value / totalDeposits);
      holderBalances[holderAddressesMemory[i]].rewards += rewardAmount;
      emit HolderRewarded(holderAddressesMemory[i], rewardAmount);
    }
  }
  function withdraw() external {
    require(holderBalances[msg.sender].isTracked, "You cannot withdraw as you are not part of ETHPool");
    uint totalToWithdraw = holderBalances[msg.sender].deposits + holderBalances[msg.sender].rewards;
    require(totalToWithdraw > 0, "You dont have anything to withdraw");

    payable(msg.sender).transfer(totalToWithdraw);
    emit HolderWithdrawal(msg.sender, holderBalances[msg.sender].deposits, holderBalances[msg.sender].rewards);

    deleteHolder(msg.sender);

    totalDeposits -= holderBalances[msg.sender].deposits;
    holderBalances[msg.sender].isTracked = false;
    holderBalances[msg.sender].deposits = 0;
    holderBalances[msg.sender].rewards = 0;

  }

  function deleteHolder(address holder) private {
    uint indexToBeDeleted = holderBalances[holder].holderIndex;
    uint arrayLength = holderAddresses.length;

    // if index to be deleted is not the last index, swap position.
    if (indexToBeDeleted < arrayLength-1) {
      address lastHolder = holderAddresses[arrayLength-1];
      holderAddresses[indexToBeDeleted] = lastHolder;
      holderBalances[lastHolder].holderIndex = indexToBeDeleted;
    }
    holderAddresses.pop();
  }
}
